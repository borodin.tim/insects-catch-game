const screens = document.querySelectorAll('.screen');
const chooseInsectBtns = document.querySelectorAll('.choose-insect-btn');
const startBtn = document.getElementById('start-btn');
const gameContainer = document.getElementById('game-container');
const insectsContainer = document.getElementById('insects-container');
const timeEl = document.getElementById('time');
const scoreEl = document.getElementById('score');
const messageEl = document.getElementById('message');
const restartBtn = document.getElementById('restart-btn');

let seconds = 0;
let score = 0;
let selectedInsect = [];
let insectsAmount = 0;
let startGameInterval;
let createInsectTimeouts = [];

restartBtn.addEventListener('click', () => {
    createInsectTimeouts.forEach(timeout => clearTimeout(timeout));
    clearInterval(startGameInterval);

    message.classList = 'message';

    screens[1].classList.remove('up');
    screens[2].classList.add('down');

    seconds = 0;
    timeEl.innerHTML = 'Time: 00:00';
    score = 0;
    setScoreInHTMLElement(0);
    selectedInsect = [];
    insectsAmount = 0;
    insectsContainer.innerHTML = '';
})

startBtn.addEventListener('click', () => {
    screens[0].classList.add('up');
});

chooseInsectBtns.forEach(btn => btn.addEventListener('click', () => {
    const imgs = btn.querySelectorAll('img');
    imgs.forEach(img => {
        const src = img.getAttribute('src');
        const alt = img.getAttribute('alt');
        selectedInsect.push({ src, alt });
    })
    insectsAmount = imgs.length;
    screens[2].classList.remove('down')
    screens[1].classList.add('up');
    createInsectTimeouts.push(setTimeout(createInsect, 1000));
    startGame();
}))

function startGame() {
    startGameInterval = setInterval(increaseTime, 1000);
}

function increaseTime() {
    let m = Math.floor(seconds / 60);
    let s = seconds % 60;
    m = m < 10 ? `0${m}` : m;
    s = s < 10 ? `0${s}` : s;
    timeEl.innerHTML = `Time: ${m}:${s}`;
    seconds++;
}

function createInsect() {
    const insect = document.createElement('div');
    insect.classList.add('insect');
    insect.classList.add('noselect');
    const { x, y } = getRandomlocation();
    insect.style.top = `${y}px`;
    insect.style.left = `${x}px`;
    const randomInsect = Math.floor(Math.random() * insectsAmount);
    insect.innerHTML = `
        <img src="${selectedInsect[randomInsect].src}" alt="${selectedInsect[randomInsect].alt}" style="transform: rotate(${Math.random() * 360}deg)" draggable="false" />
        `;
    insect.addEventListener('click', catchInsect);

    insectsContainer.appendChild(insect);
}

function getRandomlocation() {
    const width = window.innerWidth;
    const height = window.innerHeight;

    const iCw = insectsContainer.offsetWidth;
    const iCh = insectsContainer.offsetHeight;

    const x = Math.random() * iCw;
    const y = Math.random() * iCh;
    return { x, y };
}

function catchInsect() {
    increateScore();
    this.classList.add('caught');
    setTimeout(() => this.remove(), 2000);
    addInsects();
}

function addInsects() {
    createInsectTimeouts.push(setTimeout(createInsect, 1000));
    createInsectTimeouts.push(setTimeout(createInsect, 1500));
}

function increateScore() {
    score++;
    if (score > 14 && !message.classList.contains('visible')) {
        message.classList.add('visible');
        removeMessageWithTimeout();
    }
    setScoreInHTMLElement();
}

function setScoreInHTMLElement(value) {
    scoreEl.innerHTML = value ? `Score: ${value}` : `Score: ${score}`;
}

function removeMessageWithTimeout() {
    setTimeout(() => {
        if (!screens[2].classList.contains('down')) {
            message.classList.add('remove');
            setTimeout(() => message.classList.remove('visible'), 400);
            setTimeout(() => message.classList.remove('remove'), 400);
        }
    }, 7000);
}

